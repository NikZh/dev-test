function testReverse(url) {
    if (jQuery) {
        jQuery.ajax({
                method: "GET",
                data: {isAjax: true},
                url: url
            })
            .done(function (resp) {
                jQuery('.test-products-block').replaceWith(resp);
            });
        return;
    }
    setLocation(url);
}
