<?php

/**
 * Products block
 *
 * @category   Vaimo
 * @package    Vaimo_Test
 */
class Vaimo_Test_Block_Products extends Mage_Core_Block_Template
{
    /**
     * Default collection order
     */
    const DEFAULT_ORDER = 'asc';

    /**
     * @var null|Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_collection = null;

    /**
     * Prepare product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProducts()
    {
        if (is_null($this->_collection)) {
            $this->_collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(array('name', 'small_image', 'media_gallery'))
                ->addAttributeToFilter('position', array('notnull' => true))
                ->setOrder('position', $this->_getPositionOrder());
        }
        return $this->_collection;
    }


    /**
     * @return string
     */
    public function getReverseUrl()
    {
        return Mage::getUrl('vaimo_test', array('_query' => array('isReverse' => true)));
    }

    /**
     * Switch sort order
     *
     * @return $this
     */
    public function switchOrder()
    {
        $order = $this->_getPositionOrder();
        $newOrder = 'asc';
        if ($order == $newOrder) {
            $newOrder = 'desc';
        }
        Mage::getSingleton('catalog/session')->setData('test_order', $newOrder);
        return $this;
    }

    /**
     * Reset sort order
     *
     * @return $this
     */
    public function resetOrder()
    {
        Mage::getSingleton('catalog/session')->setData('test_order', self::DEFAULT_ORDER);
        return $this;
    }

    /**
     * Get sort order
     *
     * @return string
     */
    protected function _getPositionOrder()
    {
        $order = Mage::getSingleton('catalog/session')->getData('test_order');
        if (is_null($order)) {
            $order = self::DEFAULT_ORDER;
            Mage::getSingleton('catalog/session')->setData('test_order', $order);
        }
        return $order;
    }
}
