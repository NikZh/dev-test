<?php
/** @var $this Vaimo_Test_Model_Resource_Setup */
$installer = $this;
$attributeCode = 'position';
$attribute = array(
    'label'                   => 'position',
    'input'                   => 'text',
    'type'                    => 'varchar',
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                 => true,
    'visible_on_front'        => true,
    'used_in_product_listing' => true,
    'required'                => false,
    'unique'                  => false,
    'user_defined'            => true,
    'frontend_class'          => 'validate-digits'
);
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, $attribute);

$entityTypeId = (int)$installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
$sets = Mage::getModel('eav/entity_attribute_set')->getCollection()->setEntityTypeFilter($entityTypeId);

/** @var Mage_Eav_Model_Entity_Attribute_Set $set */
foreach ($sets as $set) {
    $installer->addAttributeToGroup(
        $entityTypeId, $set->getAttributeSetId(), 'General', $attributeCode, '100'
    );
}

$installer->createTestProducts(
    array(
        array(
            'name'     => 'Product 1',
            'position' => 1,
            'image'    => 'Yosemite.jpg'
        ),
        array(
            'name'     => 'Product 2',
            'image'    => 'Yosemite-2.jpg'
        ),
        array(
            'name'     => 'Product 3',
            'position' => 2,
            'image'    => 'Yosemite-3.jpg'
        ),
        array(
            'name'     => 'Product 4',
            'image'    => 'Yosemite-4.jpg'
        ),
        array(
            'name'     => 'Product 5',
            'position' => 3,
            'image'    => 'Yosemite-5.jpg'
        )
    )
);
