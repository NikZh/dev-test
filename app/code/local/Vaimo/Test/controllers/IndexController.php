<?php

/**
 * Test task controller
 *
 * @category   Vaimo
 * @package    Vaimo_Test
 */
class Vaimo_Test_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Test page action
     */
    public function indexAction()
    {
        $this->loadLayout();

        /** @var Vaimo_Test_Block_Products $block */
        $block = $this->getLayout()->getBlock('vaimo_test_products');
        if ($this->getRequest()->getParam('isReverse', false)) {
            $block->switchOrder();
        } else {
            $block->resetOrder();
        }

        if ($this->getRequest()->getParam('isAjax', false)) {
            return $this->getResponse()->setBody($block->toHtml());
        }

        $this->getLayout()->getBlock('head')->setTitle($this->__('Vaimo test page'));
        $this->renderLayout();
    }
}
