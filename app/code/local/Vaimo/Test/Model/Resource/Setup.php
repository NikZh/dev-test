<?php

/**
 * Setup model
 *
 * @category   Vaimo
 * @package    Vaimo_Test
 */
class Vaimo_Test_Model_Resource_Setup extends Mage_Catalog_Model_Resource_Setup
{
    const TEST_WEBSITE_ID       = 1;
    const TEST_TAX_CLASS        = 0;
    const TEST_PROD_DESC        = 'Test description';
    const TEST_PROD_SH_DESC     = 'Test short description';
    const TEST_IMAGE_IMPORT_DIR = 'vaimo-import';

    /**
     * Create test products
     *
     * @param array $products
     */
    public function createTestProducts($products)
    {
        Mage::app()->getStore()->setWebsiteId(self::TEST_WEBSITE_ID);
        $defaultAttributeSetId = Mage::getSingleton('eav/config')
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getDefaultAttributeSetId();
        foreach ($products as $item) {
            $product = Mage::getModel('catalog/product')
                ->setWebsiteIds(array(self::TEST_WEBSITE_ID))
                ->setAttributeSetId($defaultAttributeSetId)
                ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
                ->setSku(strtolower($item['name']))
                ->setName($item['name'])
                ->setWeight(0)
                ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->setTaxClassId(self::TEST_TAX_CLASS)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->setPrice(mt_rand(100, 9999) / 100)
                ->setDescription(self::TEST_PROD_DESC)
                ->setShortDescription(self::TEST_PROD_SH_DESC)
                ->setStockData(array(
                        'use_config_manage_stock' => 0,
                        'manage_stock'            => 0,
                        'is_in_stock'             => 1,
                    )
                );
            if (isset($item['position'])) {
                $product->setPosition($item['position']);
            }
            $product->save();
        }
    }
}
